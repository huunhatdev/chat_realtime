import React, { useState } from 'react';

export default function ChatBox({ socketRef, mess, listUser, name }) {
	const [msg, setMsg] = useState('');

	const handleType = (e) => {
		let value = e.target.value;

		setMsg(value);
	};

	const handleSend = () => {
		if (msg.trim()) socketRef.current.emit('sendMsg', msg);
		setMsg('');
	};

	const handleKeyUp = (e) => {
		if (e.key === 'Enter') {
			handleSend();
		}
	};

	return (
		<div className=''>
			<div className='flex justify-center items-center h-full '>
				<div className='w-80 h-96 bg-white rounded shadow-2xl'>
					<nav className='w-full h-10 bg-gray-900 rounded-tr rounded-tl flex justify-between items-center'>
						<div className='flex justify-center items-center flex-wrap'>
							{listUser.map((e, i) => {
								return (
									<span
										className='text-xs font-medium text-gray-300 ml-1'
										key={i}
									>
										{e.name}
									</span>
								);
							})}
						</div>
						<div className='flex items-center'>
							<i className='mdi mdi-video text-gray-300 mr-4' />{' '}
							<i className='mdi mdi-phone text-gray-300 mr-2' />{' '}
							<i className='mdi mdi-dots-vertical text-gray-300 mr-2' />{' '}
						</div>
					</nav>
					<div
						className='overflow-auto px-1 py-1'
						style={{ height: '19rem' }}
						id='journal-scroll'
					>
						{mess.map((e, i) => {
							if (e.name !== name) {
								return (
									<div className='flex items-center pt-2 pr-10' key={i}>
										<img
											src='https://i.imgur.com/IAgGUYF.jpg'
											className='rounded-full shadow-xl'
											width={15}
											height={15}
											style={{ boxShadow: '' }}
										/>
										<span
											className='flex ml-1 h-auto bg-slate-100 text-gray-900 text-xs font-normal rounded-sm px-1 p-1 items-end'
											style={{ fontSize: 10 }}
										>
											{e.name} : {e.content}
											<span
												className='text-gray-400 pl-1'
												style={{ fontSize: 8 }}
											>
												{e.time}
											</span>
										</span>
									</div>
								);
							} else {
								return (
									<div className='flex justify-end pt-2 pl-10' key={i}>
										<span
											className='bg-pink-400 h-auto text-white text-xs font-normal rounded-sm px-1 p-1 items-end flex justify-end '
											style={{ fontSize: 10 }}
										>
											{e.content}
											<span
												className='text-gray-700 pl-1'
												style={{ fontSize: 8 }}
											>
												{e.time}
											</span>
										</span>{' '}
									</div>
								);
							}
						})}

						<div className=' ' id='chatmsg'>
							{' '}
						</div>
					</div>
					<div className='flex justify-between items-center p-1 '>
						<div className='relative'>
							<i
								className='mdi mdi-emoticon-excited-outline absolute top-1 left-1 text-gray-400'
								style={{ fontSize: '17px !important', fontWeight: 'bold' }}
							/>{' '}
							<input
								type='text'
								value={msg}
								className='rounded-full pl-6 pr-12 py-2 focus:outline-none h-auto placeholder-gray-100 bg-gray-900 text-white'
								style={{ fontSize: 11, width: 250 }}
								placeholder='Type a message...'
								id='typemsg'
								onChange={handleType}
								onKeyUp={handleKeyUp}
							/>{' '}
							<i className='mdi mdi-paperclip absolute right-8 top-1 transform -rotate-45 text-gray-400' />{' '}
							<i className='mdi mdi-camera absolute right-2 top-1 text-gray-400' />{' '}
						</div>
						<div className='w-7 h-7 rounded-full bg-blue-300 text-center items-center flex justify-center hover:bg-gray-900 hover:text-white'>
							<i className='mdi mdi-microphone ' />{' '}
						</div>
						<div className='w-7 h-7 rounded-full bg-blue-300 text-center items-center flex justify-center'>
							<button
								className='w-7 h-7 rounded-full text-center items-center flex justify-center focus:outline-none hover:bg-gray-900 hover:text-white'
								onClick={handleSend}
							>
								<i className='mdi mdi-send ' />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
