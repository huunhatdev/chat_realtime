import { Button, Divider, Input, message } from 'antd';
import React, { useState, useEffect, useRef } from 'react';
import socketIOClient from 'socket.io-client';
import ChatBox from './components/ChatBox';

const host = 'http://localhost:3001';

function App() {
	const [mess, setMess] = useState([]);
	const [color, setColor] = useState('transparent');
	const [name, setName] = useState();
	const [isLogin, setIsLogin] = useState(false);
	const [listUser, setListUser] = useState([]);

	const socketRef = useRef();

	const handleSend = () => {
		socketRef.current.emit('setName', name);
	};

	const handleLogout = () => {
		socketRef.current.disconnect();
		setIsLogin(false);
	};

	useEffect(() => {
		socketRef.current = socketIOClient.connect(host);

		socketRef.current.on('noti', (data) => {
			message.info(data);
		});
		socketRef.current.on('error', (data) => {
			message.error(data);
		});
		socketRef.current.on('isLogin', (data) => {
			setIsLogin(data);
		});
		socketRef.current.on('reciveMsg', (data) => {
			setMess((state) => [...state, JSON.parse(data)]);
		});
		socketRef.current.on('listUser', (data) => {
			setListUser(data);
		});

		return () => {
			socketRef.current.disconnect();
		};
	}, []);

	useEffect(() => {
		console.log(mess);
	}, [mess]);
	return (
		<div>
			{!isLogin ? (
				<div className=' w-full flex justify-items-center space-x-5 p-5'>
					<label htmlFor='user'>Nhập tên</label>
					<input
						id='user'
						className=' border border-green-400 pl-2'
						onChange={(e) => setName(e.target.value)}
					/>
					<Button type='primary' danger onClick={handleSend}>
						Tham gia đoạn chat
					</Button>
				</div>
			) : (
				<div className=''>
					<span>Xin chào, {name} ! </span>
					<Button type='primary' danger onClick={handleLogout}>
						Đăng xuất
					</Button>
				</div>
			)}
			<Divider />
			{isLogin && (
				<ChatBox
					socketRef={socketRef}
					mess={mess}
					listUser={listUser}
					name={name}
				/>
			)}
		</div>
	);
}

export default App;
