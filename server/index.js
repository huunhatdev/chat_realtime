let express = require('express');
const http = require('http');
const moment = require('moment');
let app = express();
const PORT = 3001;
app.use(express.static('./public'));
const server = http.createServer(app);

const io = require('socket.io')(server, {
	cors: {
		origin: '*',
	},
});

const listUser = [];

//handle khi có connect từ client
io.on('connection', (socket) => {
	console.log('New client connected ' + socket.id);

	//setName vào phòng
	socket.on('setName', (name) => {
		let i = listUser.findIndex((e) => e.name === name);
		if (i > -1) {
			socket.emit('error', `Tên ${name} đã có người sử dụng`);
		} else {
			listUser.push({ name: name, id: socket.id });
			socket.broadcast.emit('noti', `${name} đã tham gia phòng chat`);
			io.sockets.emit('listUser', listUser);
			socket.emit('isLogin', true);
		}
		console.log(name + 'đăng kí');
	});

	//send msg
	socket.on('sendMsg', (data) => {
		let i = listUser.findIndex((e) => e.id === socket.id);

		let obj = {
			name: listUser[i]['name'],
			content: data,
			time: moment(new Date()).format('hh:mm a'),
		};
		console.log(obj);
		io.sockets.emit('reciveMsg', JSON.stringify(obj));
	});

	socket.on('disconnect', () => {
		let i = listUser.findIndex((e) => e.id === socket.id);
		listUser.splice(i, 1);
		io.sockets.emit('listUser', listUser);

		console.log(`${socket.id} hass disconnect`);
		socket.emit('isLogin', false);
	});
});

//handle khi disconnect

server.listen(PORT, () => {
	console.log(`server đang chạy trên cổng ${PORT}`);
});
